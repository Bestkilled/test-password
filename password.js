
const checkLength = function (password) {
  return password.length >= 8 && password.length <= 25
}

const checkAlphabet = function (password) {
  const al = 'abcdefghijklmnopqrstuvwxyz'
  for (const ch of password) {
    if (al.includes(ch.toLowerCase())) return true
  }
  return false
  // return /[a-zA-Z]/.test(password)
}
const checkDigit = function (password) {
  return /[0-9]/.test(password)
}
const checkSymbol = function (password) {
  const sym = ' !"#$%&()*+,-./:;<=>?@[]^_`{|}~'
  for (const ch of password) {
    if (sym.includes(ch.toLowerCase())) return true
  }
  return false
}
const checkPassword = function (password) {
  return checkLength(password) && checkAlphabet(password) && checkDigit(password) && checkSymbol(password)
}

module.exports = {
  checkAlphabet,
  checkLength,
  checkDigit,
  checkSymbol,
  checkPassword
}
