const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('test password length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })
  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })
  test('should 25 characters to be true', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })
  test('should 26 characters to be false', () => {
    expect(checkLength('11111111111111111111111112')).toBe(false)
  })
})

describe('test password Alphabet', () => {
  test('should has alphabet in password', () => {
    expect(checkAlphabet('v')).toBe(true)
  })
  test('should hasn not alphabet in password', () => {
    expect(checkAlphabet('11')).toBe(false)
  })
})

describe('test password digit', () => {
  test('should has digit in password', () => {
    expect(checkDigit('11222')).toBe(true)
  })
  test('should hasn not digit in password', () => {
    expect(checkDigit('w')).toBe(false)
  })
})

describe('test password symbol', () => {
  test('should has symbol in password', () => {
    expect(checkSymbol('?@[')).toBe(true)
  })
  test('should hasn not symbol in password', () => {
    expect(checkSymbol('1w')).toBe(false)
  })
})

describe('test password', () => {
  test('Should xyz!1235 to be true', () => {
    expect(checkPassword('xyz!1235')).toBe(true)
  })
  test('Should 444!1235 to be false', () => {
    expect(checkPassword('444!1235')).toBe(false)
  })
  test('Should xyz71235 to be false', () => {
    expect(checkPassword('xyz71235')).toBe(false)
  })
  test('Should xyz!hhhh to be false', () => {
    expect(checkPassword('xyz!hhhh')).toBe(false)
  })
  test('Should 77771235 to be false', () => {
    expect(checkPassword('77771235')).toBe(false)
  })
  test('Should xyz71235111111111111111111! to be false', () => {
    expect(checkPassword('xyz71235111111111111111111!')).toBe(false)
  })
})
